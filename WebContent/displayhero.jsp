<%@page import="lect06.Hero"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Display hero</title>
</head>
<body>
<h1>Hero Info</h1>
<%
	Hero h = (Hero) request.getAttribute("hero");
	if(h != null){
%>
<div>ID: <%=h.getIdhero() %></div>
<div>Name: <%=h.getName() %></div>
<div>Power: <%=h.getPower() %></div>
<%
	}
%>
</body>
</html>