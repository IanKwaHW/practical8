package dbaccess;

import java.sql.*;

import java.util.*;

public class UserDetailsDB {
		public ArrayList<UserDetails> search(int searchField){
			try {
				Class.forName("com.mysql.jdbc.Driver");
				String connURL = "jdbc:mysql://localhost/heroschema?user=root&password=T0042062F";
				Connection conn = DriverManager.getConnection(connURL);
				String sqlStr = "SELECT * FROM user_details where UserID= "+ "?";
				
				PreparedStatement pstmt = conn.prepareStatement(sqlStr);
				
				pstmt.setString(1,"%"+searchField+"%");
				ResultSet rs = pstmt.executeQuery();
				
				ArrayList<UserDetails> al= new ArrayList<UserDetails>();
				int id; 
				int age;
				String gender;
				while(rs.next()){
					id = rs.getInt("UserID");
					age = rs.getInt("Age");
					gender = rs.getString("Gender");
					
					UserDetails User=new UserDetails(id,age,gender);
					
					al.add(User);
				}
				conn.close();
				return al;
			}catch(Exception ex) {
				System.err.println(ex.getMessage());
				return null;
			}
		}
	}


