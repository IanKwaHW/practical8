package dbaccess;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbaccess.*;
import java.util.*;

/**
 * Servlet implementation class GetHeroServlet
 */
@WebServlet("/GetUserDetailsServlet")
public class GetUserDetailsServlet extends HttpServlet {private static final long serialVersionUID = 1L;

	public GetUserDetailsServlet() {
		super();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int searchString=Integer.parseInt(request.getParameter("searchString"));
		UserDetailsDB db = new UserDetailsDB();
		ArrayList<UserDetails> al = db.search(searchString); 
		request.setAttribute("SearchItems", al);
		RequestDispatcher rd=request.getRequestDispatcher("displaydetails.jsp");
		rd.forward(request,response);
	}
		
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int searchString=Integer.parseInt(request.getParameter("searchString"));
		UserDetailsDB db = new UserDetailsDB();
		ArrayList<UserDetails> al = db.search(searchString); 
		request.setAttribute("SearchItems", al);
		RequestDispatcher rd=request.getRequestDispatcher("displaydetails.jsp");
		rd.forward(request,response);
		}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
}
