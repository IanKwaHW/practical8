package lect06;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GetHeroServlet
 */
@WebServlet("/GetHeroServlet")
public class GetHeroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	Hero hero = null;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetHeroServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String id = request.getParameter("id");
		hero = HeroDB.getHero(Integer.parseInt(id));
		
		request.setAttribute("hero", hero);
		RequestDispatcher rd = request.getRequestDispatcher("displayhero.jsp");
		rd.forward(request, response);
	}

}
